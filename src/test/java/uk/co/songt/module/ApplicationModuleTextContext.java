package uk.co.songt.module;


import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import uk.co.songt.juice.module.ServiceModule;

import java.util.Properties;

import static com.google.inject.name.Names.bindProperties;

public class ApplicationModuleTextContext extends AbstractModule {

    @Override
    protected void configure() {
        binder().bind(Properties.class).toProvider(PropertiesTestProvider.class).in(Singleton.class);
        bindProperties(binder(), PropertiesTestProvider.getProperties());
        install(new ServiceModule());
    }

}
