package uk.co.songt;

import org.junit.Test;
import uk.co.songt.exception.FileContentExceptoion;
import uk.co.songt.imp.FileReaderImp;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

/**
 * Created by tsong on 29/06/2016.
 */
public class FileReaderImpTest {
    private static final long MEGABYTE = 1024L * 1024L;

    public static long bytesToMegabytes(long bytes) {
        return bytes / MEGABYTE;
    }
    final String BASEDIR = "src/test/testFiles/";
    @Test(expected=FileContentExceptoion.class)
    public void testParseFileException() {
        FileReaderImp fileReaderImp = new FileReaderImp();
        CarSale[] carSales =  fileReaderImp.parseFile(BASEDIR+"nofile.csv");
    }

    @Test
    public void testParseFile() {
        FileReaderImp fileReaderImp = new FileReaderImp();
        CarSale[] carSales =  fileReaderImp.parseFile(BASEDIR+"carModel.csv");
        assert  carSales.length==5;
    }

    @Test
    public void testParseLargeFile() {
        FileReaderImp fileReaderImp = new FileReaderImp();
        CarSale[] carSales =  fileReaderImp.parseFile(BASEDIR+"carModelLarge.csv");
        Runtime runtime = Runtime.getRuntime();
        runtime.gc();
        long memory = runtime.totalMemory() - runtime.freeMemory();
        System.out.println("Used memory is bytes: " + memory);
        System.out.println("Used memory is megabytes: "+bytesToMegabytes(memory));
        assert  carSales.length>0;
    }

    @Test(expected=IOException.class)
    public   void testReadFile() throws IOException{
        FileReaderImp fileReaderImp = new FileReaderImp();
        fileReaderImp.readFile("carModel.csv");
    }

    @Test(expected=FileContentExceptoion.class)
    public  void testEmptryFile() throws FileContentExceptoion{
        FileReaderImp fileReaderImp = new FileReaderImp();
        fileReaderImp.checkFileEmpty("");
    }
    @Test
    public  void testNOEmptryFile() throws FileContentExceptoion{
        FileReaderImp fileReaderImp = new FileReaderImp();
        assert  fileReaderImp.checkFileEmpty("Model, City") == "Model, City";
    }

    @Test(expected=FileContentExceptoion.class)
    public   void testEmptryFileThrowException() throws FileContentExceptoion,IOException{
        FileReaderImp fileReaderImp = new FileReaderImp();
        BufferedReader reader = fileReaderImp.readFile(BASEDIR+"carModelEmpty.csv");
        fileReaderImp.getLineCount(reader);
    }
    @Test
    public   void testNonEmptryFile() throws FileContentExceptoion,IOException{
        FileReaderImp fileReaderImp = new FileReaderImp();
        BufferedReader reader = fileReaderImp.readFile(BASEDIR+"carModel.csv");
        assert fileReaderImp.getLineCount(reader)==5;
    }

    @Test
    public void testWriteToFIle() throws IOException{
        final String fileName =BASEDIR+ "testWriteToFIle.csv";
        FileReaderImp fileReaderImp = new FileReaderImp();
        fileReaderImp.addToFile(fileName,"Model,City", true);
        fileReaderImp.addToFile(fileName,"polo,london",false);
        fileReaderImp.addToFile(fileName,"polo,london",false);
        BufferedReader reader = fileReaderImp.readFile(fileName);
        assert fileReaderImp.getLineCount(reader)==2;
        File file = new File(fileName);
        file.delete();
    }

}
