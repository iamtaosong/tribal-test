package uk.co.songt;


import com.google.inject.Inject;
import org.jukito.JukitoModule;
import org.jukito.JukitoRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import uk.co.songt.Sales;
import uk.co.songt.imp.SalesImp;
import uk.co.songt.module.ApplicationModuleTextContext;

import java.util.Arrays;
import java.util.Properties;
import java.util.stream.Stream;

public class SalesImpClassTest {

    @Test
    public void testGetModelTotal() {
        CarSale[] carSales = new CarSale[3];
        carSales[0]=new CarSale("golf", "london");
        carSales[1]=new CarSale("polo", "london");
        carSales[2]=new CarSale("golf", "london");
        Object[] models = Stream.of(carSales).map(i -> i.getModel()).toArray();
        assert   new SalesImp().getModelTotal(models,"golf") ==2;
    }

    @Test
    public void testGetModelWithCount() {
        String[] uniqueModelCount = new String[3];
        uniqueModelCount[0]="golf-2";
        uniqueModelCount[1]="polo-2";
        uniqueModelCount[2]="passat-1";
        Object[] modles = new SalesImp().getModelWithCount(uniqueModelCount,2);
        assert Arrays.stream(modles).anyMatch("golf"::equals);
        assert Arrays.stream(modles).anyMatch("polo"::equals);
        assert !Arrays.stream(modles).anyMatch("passat"::equals);
    }

    @Test
    public void testGetStringArrayFromObjectArray(){
        CarSale[] carSales = new CarSale[1];
        carSales[0]=new CarSale("golf", "london");
        Object[] models = Stream.of(carSales).map(i -> i.getModel()).toArray();
        assert new SalesImp().getStringArrayFromObjectArray(models)[0] =="golf";
    }

    @Test
    public void testGetTopItems() {
        CarSale[] carSales = new CarSale[3];
        carSales[0]=new CarSale("golf", "london");
        carSales[1]=new CarSale("polo", "london");
        carSales[2]=new CarSale("golf", "london");
        Object[] models = Stream.of(carSales).map(i -> i.getModel()).toArray();
        assert new SalesImp().getTopItems(models)[0].equalsIgnoreCase("golf");
    }
}
