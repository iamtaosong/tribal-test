package uk.co.songt.juice.module;


import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import uk.co.songt.juice.module.providers.PropertiesProvider;

import java.util.Properties;

import static com.google.inject.name.Names.bindProperties;

public class ApplicationModule extends AbstractModule {

    @Override
    protected void configure() {
        binder().bind(Properties.class).toProvider(PropertiesProvider.class).in(Singleton.class);
        bindProperties(binder(), PropertiesProvider.getProperties());
        install(new ServiceModule());
    }

}
