package uk.co.songt.juice.module.providers;


import com.google.inject.Provider;

import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.Callable;


public class PropertiesProvider implements Provider<Properties> {

    private static Properties properties = new Properties();


    static {
        InputStream inputStream = PropertiesProvider.class
                .getClassLoader()
                .getResourceAsStream("tribal-car-sales.properties");

        uncheck(() -> {
            properties.load(inputStream);
            return null;
        });
    }
    public static <T> T uncheck(Callable<T> function) {
        try {
            return function.call();
        } catch (Exception var2) {
            throw new RuntimeException(var2);
        }
    }
    @Override
    public Properties get() {
        return properties;
    }

    public static String getProperty(String key) {
        return properties.getProperty(key);
    }

    public static Properties getProperties() {
        return properties;
    }
}

