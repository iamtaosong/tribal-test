package uk.co.songt;

public class CarSale {

    private String city;
    private String model;

    public CarSale(String model, String city){
        this.city = city;
        this.model = model;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}

