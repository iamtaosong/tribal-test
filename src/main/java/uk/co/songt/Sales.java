package uk.co.songt;

import uk.co.songt.exception.FileContentExceptoion;

public interface Sales {

    public CarSale[] getCarSales() throws FileContentExceptoion;

    public String[] getUnqueItems( Object[] modles )throws FileContentExceptoion;

    public String[] mostPopularModel()throws FileContentExceptoion;

    public String[] mostPopularModelOf(String city)throws FileContentExceptoion;

    public int sales(String model)throws FileContentExceptoion;

    public void addSale(String model,String city)throws FileContentExceptoion;
}
