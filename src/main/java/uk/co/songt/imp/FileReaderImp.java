package uk.co.songt.imp;

import uk.co.songt.CarSale;
import uk.co.songt.exception.FileContentExceptoion;

import java.io.*;
import java.util.StringTokenizer;

public class FileReaderImp implements uk.co.songt.FileReader {

    public BufferedReader readFile(String fileName) throws IOException {
        return new BufferedReader(new FileReader(fileName));
    }

    public String checkFileEmpty(String line) throws FileContentExceptoion {
        if (line == null || "".equals(line)) {
            throw new FileContentExceptoion("Empty file");
        }
        return line;
    }

    public int getLineCount(BufferedReader reader) throws FileContentExceptoion, IOException {
        int count = 0;
        String line = reader.readLine();
        try {
            checkFileEmpty(line);
            line = reader.readLine();
            while (line != null) {
                count++;
                line = reader.readLine();
            }
            return count;
        } catch (FileContentExceptoion e) {
            throw e;
        }
    }

    public CarSale[] getCarSales(String fileName, int salesCount) throws IOException {
        CarSale[] carSales = new CarSale[salesCount];
        BufferedReader reader = readFile(fileName);
        reader.readLine();
        String line = reader.readLine();
        int count = 0;
        while (line != null) {
            StringTokenizer tokenizer = new StringTokenizer(line, ",");
            String model = tokenizer.nextToken().trim();
            String city = tokenizer.nextToken().trim();
            carSales[count++] = new CarSale(model, city);
            line = reader.readLine();
        }
        return carSales;
    }
    public   CarSale[] parseFile(String fileName) throws FileContentExceptoion {
        BufferedReader reader = null;
        CarSale[] carSales;
        String line;
        try {
            reader = readFile(fileName);
            int count = getLineCount(reader);
            return getCarSales(fileName, count);
        } catch (FileContentExceptoion e) {
            throw new FileContentExceptoion("File not found");
        } catch (FileNotFoundException e) {
            throw new FileContentExceptoion("File not found");
        } catch (IOException e) {
            throw new FileContentExceptoion("Unable to read from the file specified");
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    throw new FileContentExceptoion("Unable to close the input file specified: " + fileName);
                }
            }
            else{
                throw new FileContentExceptoion("Unable to read from the file specified");
            }
        }
    }

    public void  addToFile(String fileName, String line, boolean newFile) throws FileContentExceptoion {
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new FileWriter(fileName,true));
            if(!newFile){
                out.newLine();
            }

            out.write(line);

        } catch (FileNotFoundException e) {
            throw new FileContentExceptoion("File not found");
        } catch (IOException e) {
            throw new FileContentExceptoion("Unable to read from the file specified");
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    throw new FileContentExceptoion("Unable to close the input file specified: " + fileName);
                }
            }
            else{
                throw new FileContentExceptoion("Unable to read from the file specified");
            }
        }

    }
}


